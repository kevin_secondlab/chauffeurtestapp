package chauffeurapp.secondlab.com.chauffeurapp.data;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import chauffeurapp.secondlab.com.chauffeurapp.pojo.Rider;
import chauffeurapp.secondlab.com.chauffeurapp.utils.FileHelper;

/**
 * Created by admin on 12/08/16.
 */
public class Riders {

    private ArrayList<Rider> riders;

    public Riders(ArrayList<Rider> riders) {
        this.riders = riders;
    }

    public static Riders newInstance(Context context) {
        try {
            // read json
            JSONArray ridersJsonArray = new JSONArray(FileHelper.readFromAsset(context, "riders.json"));
            return new Riders(Rider.fromJson(ridersJsonArray));
        } catch (JSONException jsonEx) {
            jsonEx.printStackTrace();
        }

        return new Riders(new ArrayList<Rider>());
    }

    public ArrayList<Rider> getRiders() {
        return riders;
    }

    public static ArrayList<Rider> findRiderByName(ArrayList<Rider> riders, String name) {
        ArrayList<Rider> result = new ArrayList<>();

        if (riders.isEmpty()) {
            return result;
        }

        if (name.isEmpty()) {
            return result;
        }

        name = name.toLowerCase();
        for (Rider rider: riders) {
            if (rider.getLastname().contains(name) || rider.getFirstname().contains(name)) {
                result.add(rider);
            }
        }

        return result;
    }
}

package chauffeurapp.secondlab.com.chauffeurapp.pojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by admin on 12/08/16.
 */
public class Rider {

    private String id;
    private String lastname;
    private String firstname;
    private String state;

    public Rider(String id, String lastname, String firstname, String state) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getState() {
        return state;
    }

    public static ArrayList<Rider> fromJson(JSONArray jsonArray) {
        ArrayList<Rider> riders = new ArrayList<>();
        int size = jsonArray.length(), i=0;
        for (;i<size; i++) {
            try {
                Rider rider = Rider.fromJson(jsonArray.getJSONObject(i));
                if (rider != null) {
                    riders.add(rider);
                }
            } catch (JSONException jsonEx) {
                jsonEx.printStackTrace();
            }
        }

        return riders;
    }

    public static Rider fromJson(JSONObject jsonObject) {
        try {
            String lastname = jsonObject.getString("lastname").toLowerCase();
            String firstname = jsonObject.getString("firstname").toLowerCase();
            String id = jsonObject.getString("id");
            String state = jsonObject.getString("state");

            return new Rider(
                    id,
                    lastname,
                    firstname,
                    state
            );
        } catch (JSONException jsonEx) {
            jsonEx.printStackTrace();
        }

        return null;
    }
}

package chauffeurapp.secondlab.com.chauffeurapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import chauffeurapp.secondlab.com.chauffeurapp.R;
import chauffeurapp.secondlab.com.chauffeurapp.pojo.Rider;

public class RiderAdapter extends ArrayAdapter<Rider> {

    class ViewHolder {
        TextView lastname;
        TextView firstname;
        TextView state;
    }

    public RiderAdapter(Context context, int resource, List<Rider> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            viewHolder = new ViewHolder();
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.list_item_rider, parent, false);
            viewHolder.lastname = (TextView) convertView.findViewById(R.id.lastname);
            viewHolder.firstname = (TextView)  convertView.findViewById(R.id.firstname);
            viewHolder.state = (TextView) convertView.findViewById(R.id.state);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Rider rider = getItem(position);
        viewHolder.lastname.setText(rider.getLastname());
        viewHolder.firstname.setText(rider.getFirstname());
        viewHolder.state.setText(rider.getState());

        return convertView;
    }

}

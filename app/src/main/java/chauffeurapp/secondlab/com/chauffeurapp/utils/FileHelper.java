package chauffeurapp.secondlab.com.chauffeurapp.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by admin on 12/08/16.
 */
public class FileHelper {

    public static String readFromAsset(Context context, String fileName)
    {
        String text = "";
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            text = new String(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}

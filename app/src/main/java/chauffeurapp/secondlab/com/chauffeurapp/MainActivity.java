package chauffeurapp.secondlab.com.chauffeurapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import chauffeurapp.secondlab.com.chauffeurapp.adapters.RiderAdapter;
import chauffeurapp.secondlab.com.chauffeurapp.data.Riders;
import chauffeurapp.secondlab.com.chauffeurapp.pojo.Rider;

public class MainActivity extends AppCompatActivity {

    private EditText searchRider;
    private Button searchAction;
    private ListView ridersList;
    private RiderAdapter ridersAdapter;
    private Riders riders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        searchAction = (Button) findViewById(R.id.searchAction);
        searchRider = (EditText) findViewById(R.id.searchRider);
        ridersList = (ListView) findViewById(R.id.riders);

        riders = Riders.newInstance(this);
        setAdapters();
        setListeners();
    }

    private void setAdapters() {
        ridersAdapter = new RiderAdapter(this, R.layout.list_item_rider, new ArrayList<Rider>());
        ridersList.setAdapter(ridersAdapter);
    }

    private void setListeners() {
        searchAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchRider.getText().toString().isEmpty()) {
                    return;
                }
                searchRider();
            }
        });

        TextWatcher searchWatcher = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isEmpty()) {
                    clearList();
                    return;
                }

                if (filterLongEnough()) {
                    searchRider();
                }
            }

            private boolean filterLongEnough() {
                return searchRider.getText().toString().trim().length() > 3;
            }

            private boolean isEmpty() {
                return searchRider.getText().toString().isEmpty();
            }
        };

        searchRider.addTextChangedListener(searchWatcher);
    }

    private void updateRiders(ArrayList<Rider> riders) {
        ridersAdapter.clear();
        ridersAdapter.addAll(riders);
        ridersAdapter.notifyDataSetChanged();
    }

    private void clearList() {
        updateRiders(new ArrayList<Rider>());
    }

    private void searchRider() {
        updateRiders(Riders.findRiderByName(riders.getRiders(), searchRider.getText().toString()));
    }
}
